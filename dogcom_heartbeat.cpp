#include "dogcom_heartbeat.h"
#include "ui_dogcom_heartbeat.h"

dogcom_heartbeat::dogcom_heartbeat(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::dogcom_heartbeat)
{
    ui->setupUi(this);
    connect(ui->startButton,SIGNAL(clicked()),this,SLOT(on_startButton_clicked()));
    connect(ui->endButton,SIGNAL(clicked()),&pro_dogcom,SLOT(terminate()));
    connect(ui->exitButton,SIGNAL(clicked()),&pro_dogcom,SLOT(terminate()));
    connect(&pro_dogcom,SIGNAL(stateChanged(QProcess::ProcessState)),this,SLOT(dogcom_started()));
    connect(&pro_dogcom,SIGNAL(readyReadStandardOutput()),this,SLOT(refresh_monitor()));
    connect(&pro_dogcom,SIGNAL(errorOccurred(int,QProcess::ProcessError)),this,SLOT(dogcom_error()));
}

dogcom_heartbeat::~dogcom_heartbeat()
{
    delete ui;
}
void dogcom_heartbeat::on_startButton_clicked(){
    QString program = "./dogcom";
    QStringList arguments;
    arguments << "-m" << "pppoe" << "-c" << "dogcom.conf";
    pro_dogcom.start(program,arguments);
}
void dogcom_heartbeat::refresh_monitor(){
    QByteArray qByteRead = pro_dogcom.readAllStandardOutput();
    ui->monitor->append(QString::fromLocal8Bit(qByteRead));
}
void dogcom_heartbeat::dogcom_started(){
    if(pro_dogcom.state()==QProcess::Starting){
        ui->monitor->clear();
        ui->monitor->setText(QStringLiteral("要来了(狂喜)"));
        ui->monitor->update();
    }else if(pro_dogcom.state()==QProcess::Running){
        ui->monitor->append(QStringLiteral("在鹿上了"));
    }else{
        ui->monitor->append(QStringLiteral("我好了"));
    }
}
void dogcom_heartbeat::dogcom_error(){
    ui->monitor->append(QStringLiteral("裂开了,停了(指故障)"));
}
