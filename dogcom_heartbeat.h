#ifndef DOGCOM_HEARTBEAT_H
#define DOGCOM_HEARTBEAT_H

#include <QMainWindow>
#include <qprocess.h>
QT_BEGIN_NAMESPACE
namespace Ui { class dogcom_heartbeat; }
QT_END_NAMESPACE

class dogcom_heartbeat : public QMainWindow
{
    Q_OBJECT
public:
    dogcom_heartbeat(QWidget *parent = nullptr);
    ~dogcom_heartbeat();

private:
    Ui::dogcom_heartbeat *ui;
    QProcess pro_dogcom;
private slots:
    void on_startButton_clicked();
    void refresh_monitor();
    void dogcom_started();
    void dogcom_error();
};
#endif // DOGCOM_HEARTBEAT_H
