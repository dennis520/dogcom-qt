#include "dogcom_heartbeat.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    dogcom_heartbeat w;
    w.show();
    return a.exec();
}
